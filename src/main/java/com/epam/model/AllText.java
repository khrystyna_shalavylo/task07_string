package com.epam.model;

import java.util.List;

public class AllText {

    private StringBuilder allText;

    public AllText() {
        allText=new StringBuilder();
    }

    public AllText(List<Sentence> sentences) {
        for (Sentence s : sentences) {
            allText.append(s);
        }
    }

    public StringBuilder getAllText() {
        return this.allText;
    }

    public void setAllText(List<Sentence> sentences) {
        for (Sentence s : sentences) {
            allText.append(s);
        }
    }

    @Override
    public final String toString() {
        return "{" + allText.toString() + "}";
    }
}
