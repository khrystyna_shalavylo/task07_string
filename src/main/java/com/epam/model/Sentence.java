package com.epam.model;

import java.util.List;
import static com.epam.utils.Punctuation.POINT;
import static com.epam.utils.Punctuation.SPACE;

public class Sentence {

    private StringBuilder sentence;

    public Sentence(List<Word> words) {
        sentence=new StringBuilder();
    }

    public StringBuilder getSentence() {
        return this.sentence;
    }

//    public char getFirstChar(){
//        String sentence=getSentence().toString();
//        return sentence.charAt(0);
//    }
//
//    public char getLastChar(){
//        String sentence=getSentence().toString();
//        return sentence.charAt(sentence.length()-1);
//    }

    public void setSentence(List<Word> words) {
        String firstWord=words.get(0).toString();
        String firstWordUpperCase=firstWord.toUpperCase();
        StringBuilder start= new StringBuilder();
        start.append(firstWordUpperCase.charAt(0));
        for(int i=1;i<firstWord.length();i++){
            start.append(firstWord.charAt(i));
        }
        sentence.append(SPACE+start.toString());
        for (int i=1;i<words.size();i++) {
            sentence.append(SPACE + words.get(i).toString()) ;
        }
        sentence.append(POINT);
    }

    @Override
    public final String toString() {
        return "{" + sentence.toString() + "}";
    }
}
