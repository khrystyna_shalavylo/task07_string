package com.epam.controller;

import com.epam.model.Sentence;
import com.epam.view.InputTextFromFile;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Controller {

    private InputTextFromFile fileText;

    public Controller() {
        fileText = new InputTextFromFile();
    }

    public String sentencesBeginWithUpperCase() {
        List<Sentence> sentences = fileText.sentencesFormation();
        for (int i = 0; i < sentences.size(); i++) {
            if (!sentences.get(i).toString().startsWith("[A-Z]")) {
                return (i + 1) + "th sentence begins with lower case.";
            }
            return "All sentences begin with upper case.";
        }
        return "";
    }

    public String allVowelsReplacedWith_() {
        List<Sentence> sentences = fileText.sentencesFormation();
        String changedText = "";
        for (int i = 0; i < sentences.size(); i++) {
            Pattern p = Pattern.compile("[AEIYUOaeoiyu]+");
            Matcher m = p.matcher((CharSequence) sentences.get(i));
            if (m.find()) {
                changedText += m.replaceAll("_");
            }
        }
        return changedText;
    }

    public String allTabulationReplaced() {
        List<Sentence> sentences = fileText.sentencesFormation();
        String changedText = "";
        for (int i = 0; i < sentences.size(); i++) {
            Pattern p = Pattern.compile("[\\s]+");
            Matcher m = p.matcher((CharSequence) sentences.get(i));
            if (m.find()) {
                changedText += m.replaceAll(" ");
            }
        }
        return changedText;
    }

    public int countingSentencesWithSameWords() {
        List<Sentence> sentences = fileText.sentencesFormation();
        int countSentencesWithSameWords = 0;
        for (int i = 0; i < sentences.size(); i++) {
            List<String> words = Arrays.stream(sentences.get(i).toString()
                    .split(" "))
                    .collect(Collectors.toList());
            for (int j = 0; j < words.size(); j++) {
                for (int k = j + 1; k < words.size(); k++)
                    if (k != j && words.get(k).equals(words.get(j))) {
                        countSentencesWithSameWords++;
                        j = words.size();
                    }
            }
        }
        return countSentencesWithSameWords;
    }

    public StringBuilder sortingSentencesByRisingWordsCount() {
        List<Sentence> sentences = fileText.sentencesFormation();
        StringBuilder newText = new StringBuilder();
        int[][] sentenceSizes = new int[sentences.size()][2];
        for (int i = 0; i < sentences.size(); i++) {
            List<String> words = Arrays.stream(sentences.get(i).toString()
                    .split(" "))
                    .collect(Collectors.toList());
            sentenceSizes[i][0] = words.size();
            sentenceSizes[i][1] = i;
        }
        for (int i = 0; i < sentences.size(); i++) {
            for (int j = i + 1; j < sentences.size(); j++) {
                if (sentenceSizes[i][0] > sentenceSizes[j][0])
                    swap(sentenceSizes[i][0], sentenceSizes[j][0]);
            }
        }
        for (int i = 0; i < sentences.size(); i++) {
            if (i == sentenceSizes[i][1]) {
                newText.append(sentences.get(i));
            }
        }
        return newText;
    }

    private void swap(int first, int next) {
        int copy = first;
        first = next;
        next = copy;
    }

    public String findingUniqueWordInFirstSentence() {
        List<Sentence> sentences = fileText.sentencesFormation();
        String uniqueWord = "";
        List<String> words = Arrays.stream(sentences.get(0).toString()
                .split(" "))
                .collect(Collectors.toList());
        for (int j = 0; j < words.size(); j++) {
            uniqueWord = words.get(j);
            for (int i = 1; i < sentences.size(); i++) {
                List<String> wordsNext = Arrays.stream(sentences.get(i).toString()
                        .split(" "))
                        .collect(Collectors.toList());
                for (int e = 0; e < wordsNext.size(); e++) {
                    if (words.get(j).equals(wordsNext.get(e))) {
                        e = wordsNext.size();
                        uniqueWord = words.get(j + 1);
                        j++;
                        if (j == words.size()) {
                            return "No unique word.";
                        }
                    }
                }
            }
        }
        return uniqueWord;
    }

    public StringBuilder wordsInQuestionsWithFixedSize_(int wordSize) {
        List<Sentence> sentences = fileText.sentencesFormation();
        StringBuilder allWords = new StringBuilder();
        for (int i = 0; i < sentences.size(); i++) {
            Pattern p = Pattern.compile("[?]+");
            Matcher m = p.matcher((CharSequence) sentences.get(i));
            if (m.find()) {
                List<String> words = Arrays.stream(sentences.get(i).toString()
                        .split(" "))
                        .collect(Collectors.toList());
                for (int j = 0; j < words.size(); j++) {
                    if (words.get(j).length() == wordSize) {
                        allWords.append(words.get(j) + " ");
                    }
                }
            }
        }
        return allWords;
    }

    public StringBuilder swapFirstWordVowelWithTheLongestWord() {
        List<Sentence> sentences = fileText.sentencesFormation();
        StringBuilder changedText = new StringBuilder();
        String theBiggestWord = "";
        for (int i = 0; i < sentences.size(); i++) {
            Pattern p = Pattern.compile("[EYUOIA]{1}");
            Matcher m = p.matcher((CharSequence) sentences.get(i));
            if (m.find()) {
                List<String> words = Arrays.stream(sentences.get(i).toString()
                        .split(" "))
                        .collect(Collectors.toList());
                for (int j = 0; j < words.size(); j++) {
                    for (int k = j + 1; k < sentences.size(); k++) {
                        if (words.get(j).length() > words.get(k).length())
                            theBiggestWord = words.get(j);
                    }
                }
                swapString(words.get(0), theBiggestWord);
            }
            changedText.append(sentences.get(i));
        }
        return changedText;
    }

    private void swapString(String first, String next) {
        String copy = first;
        first = next;
        next = copy;
    }
}

