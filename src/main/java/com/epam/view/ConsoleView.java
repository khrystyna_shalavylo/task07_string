package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

public class ConsoleView implements View, Printable {

    private Locale locale;
    private ResourceBundle resourceBundle;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);
    private Controller controller;

    private ConsoleView() {
        input = new Scanner(System.in);
        controller=new Controller();
        logger.info("Chose a language \n"+"\n1)English"+"\n2)Ukrainian \n");
        try {
            int choose = input.nextInt();
            if (choose==1)
                englishMenu();
            if (choose==2)
                ukrainianMenu();
        }
        catch (Exception exception){
            logger.info("Invalid enter.");
            logger.error(exception.getStackTrace());
        }

    }

    private void putResourceBundle() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("9", resourceBundle.getString("9"));
        menu.put("10", resourceBundle.getString("10"));
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::print1);
        methodsMenu.put("2", this::print2);
        methodsMenu.put("3", this::print3);
        methodsMenu.put("4", this::print4);
        methodsMenu.put("5", this::print5);
        methodsMenu.put("6", this::print6);
        methodsMenu.put("7", this::print7);
        methodsMenu.put("8", this::print8);
        methodsMenu.put("9", this::languageMenu);
        methodsMenu.put("10", this::print10);
    }



    private void ukrainianMenu() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        showInfo();
    }

    private void englishMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        showInfo();
    }

    private void print1() {
        logger.info("Output:\n");
            controller.sentencesBeginWithUpperCase();
    }

    private void print2() {
        logger.info("Output:\n");
        controller.allVowelsReplacedWith_();
    }

    private void print3() {
        logger.info("Output:\n");
        controller.allTabulationReplaced();
    }

    private void print4() {
        logger.info("Output:\n");
        controller.countingSentencesWithSameWords();
    }

    private void print5() {
        logger.info("Output:\n");
        controller.sortingSentencesByRisingWordsCount();
    }

    private void print6() {
        logger.info("Output:\n");
        controller.findingUniqueWordInFirstSentence();
    }

    private void print7() {
        logger.info("Output:\n");
        controller.wordsInQuestionsWithFixedSize_(4);
    }
    private void print8() {
        logger.info("Output:\n");
        controller.swapFirstWordVowelWithTheLongestWord();
    }
    private int print10() {
        return -1;
    }

    private void outputMenu() {
        logger.info("\nMenu:\n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
    }
    private void languageMenu(){
        new ConsoleView();
    }

    public void print(){
        String keyMenu = input.nextLine().toUpperCase();
        String q="Q";
        try {
            if (methodsMenu.get(keyMenu).equals("1"))
                print1();
            if (methodsMenu.get(keyMenu).equals("2"))
                print2();
            if (methodsMenu.get(keyMenu).equals("3"))
                print3();
            if (methodsMenu.get(keyMenu).equals("4"))
                print4();
            if (methodsMenu.get(keyMenu).equals("5"))
                print5();
            if (methodsMenu.get(keyMenu).equals("6"))
                print6();
            if (methodsMenu.get(keyMenu).equals("7"))
                print7();
            if (methodsMenu.get(keyMenu).equals("8"))
                print8();
            if (methodsMenu.get(keyMenu).equals("9"))
                languageMenu();
            if (methodsMenu.get(keyMenu).equals("10"))
                print10();
        }
        catch (Exception exception){
            logger.info("Invalid enter.");
            logger.error(exception.getStackTrace());
        }
    }

    public void showInfo() {
       // String keyMenu;
        do {
            outputMenu();
            logger.info("\nChoose menu item:\n");
           // keyMenu = input.nextLine().toUpperCase();
            try {
                print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (print10()==-1);
    }

    public static void main(String[] args) {
        new ConsoleView().showInfo();
    }
}
