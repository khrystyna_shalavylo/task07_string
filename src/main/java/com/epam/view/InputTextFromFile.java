package com.epam.view;

import com.epam.model.AllText;
import com.epam.model.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InputTextFromFile {

    private String read;
    private AllText text;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public InputTextFromFile() {
        read = reading("C:\\Users\\Христя\\IdeaProjects\\String\\src\\main\\resources\\text");
        text = new AllText(sentencesFormation());
    }

    private String reading(String myFile) {
        String line = "";
        StringBuilder text = new StringBuilder(line);
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(myFile)))) {
            while ((line = reader.readLine()) != null) {
                text.append(line);
            }
        } catch (FileNotFoundException e) {
            logger.warn("File not found.");
        } catch (IOException e) {
            logger.warn("IO exception.");
        }
        return text.toString();
    }

    public List<Sentence> sentencesFormation(){
        List<Sentence> sentences = new ArrayList<>(Collections.singletonList((Sentence) Arrays.asList(read
                .split("[.!?]*"))));
        return sentences;
    }
}
