package com.epam.utils;

public enum Punctuation {
    POINT("."), COMMA(","), DASH("-"), SPACE(" "),
    QUESTION_MARK("?"), EXCLAMATION_MARK("!");

    private String mark;

    private Punctuation(String mark) {
        this.mark = mark;
    }

    public String getMark() {
        return mark;
    }
}
